name 'apache_django'
maintainer 'The Authors'
maintainer_email 'you@example.com'
license 'all_rights'
description 'Installs/Configures apache_django'
long_description 'Installs/Configures apache_django'
version '0.1.0'

depends 'apache2', '~> 3.1.0'

depends 'python', '~> 1.4.6'
depends 'application_python', '~> 4.0.0'
