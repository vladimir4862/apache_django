#
# Cookbook Name:: apache_django
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

include_recipe 'apache2'
include_recipe 'apache2::mod_proxy_http'
include_recipe 'apache_django::config_httpd'
include_recipe 'python'
include_recipe 'apache_django::django'
